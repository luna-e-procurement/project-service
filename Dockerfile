FROM maven:3.6.3-openjdk-17-slim
WORKDIR /app
COPY . .
RUN mvn clean install
EXPOSE 8900
ENV DATABASE_HOST=host.docker.internal
ENV DATABASE_PORT=7777
ENV DATABASE_NAME=productdb
ENV DATABASE_USERNAME=postgres
ENV DATABASE_PASSWORD=0989172935
ENV PROFILE=default
ENV FE_HOST=fe
ENV EUREKA_HOST=registry-service
ENV EUREKA_PORT=8761
ENV DB_CONFIG=update
ENTRYPOINT ["java", "-jar", "/app/target/project-service-0.0.1-SNAPSHOT.jar","--spring.profiles.active=${PROFILE}"]