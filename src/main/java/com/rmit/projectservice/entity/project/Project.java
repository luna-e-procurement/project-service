package com.rmit.projectservice.entity.project;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "project")
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull(message = "Project name must not be null")
    private String name;

    @NotNull(message = "Project code must not be null")
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "project_code_id", referencedColumnName = "id")
    private ProjectCode projectCode;

    private String value;

    @NotNull(message = "Project allowance must not be null")
    private Integer purchaseAllowance;


    @Column(columnDefinition = "boolean default false")
    private Boolean isDefault = false;

    @NotNull(message = "Project must be belong to a legal entity")
    private String legalEntityCode;
}
