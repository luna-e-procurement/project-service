package com.rmit.projectservice.entity.purchaserequisition;

public enum PurchaseRequisitionPriority {
    LOW,
    MEDIUM,
    HIGH
}
