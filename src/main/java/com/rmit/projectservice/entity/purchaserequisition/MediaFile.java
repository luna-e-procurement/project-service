package com.rmit.projectservice.entity.purchaserequisition;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "purchase_requisition_media_file")
public class MediaFile {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private Long id;

    private String src;

    @ManyToOne
    @JoinColumn(name = "purchase_requisition_id")
    @JsonIgnore
    private PurchaseRequisition purchaseRequisition;
}
