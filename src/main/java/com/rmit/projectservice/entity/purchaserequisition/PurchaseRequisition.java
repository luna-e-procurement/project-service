package com.rmit.projectservice.entity.purchaserequisition;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "purchase_requisition")
public class PurchaseRequisition {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull(message = "Project code must not be null")
    private String projectCode;


    @NotNull(message = "Project name must not be null")
    private String purchaseName;

    @NotNull(message = "Requester must not be null")
    private String requester;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "purchase_requisition_id")
    private List<Comment> comments;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "purchase_requisition_id")
    private List<MediaFile> fileUploads;

    @Enumerated(EnumType.STRING)
    private PurchaseRequisitionPriority priority = PurchaseRequisitionPriority.MEDIUM;

    @Enumerated(EnumType.STRING)
    private PurchaseRequisitionStatus status = PurchaseRequisitionStatus.DRAFT;

    private Boolean isApproved = false;
    private Boolean isRejected = false;

    private LocalDate createdDate;

    private LocalDate targetDate;

    private LocalDate dueDate;
}
