package com.rmit.projectservice.entity.purchaserequisition;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "purchase_requisition_comment")
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private Long id;

    private String content;

    private String userName;

    private LocalDateTime createdDate;

    @ManyToOne
    @JoinColumn(name = "purchase_requisition_id")
    @JsonIgnore
    private PurchaseRequisition purchaseRequisition;
}
