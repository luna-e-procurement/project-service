package com.rmit.projectservice.entity.purchaserequisition;

public enum PurchaseRequisitionStatus {
    DRAFT,
    READY,
    WAITING_TO_APPROVAL,
    TO_DO,
    IN_PROGRESS,
    ON_HOLD,
    CANCELLED,
    COMPLETED
}
