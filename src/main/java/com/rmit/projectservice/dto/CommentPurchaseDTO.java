package com.rmit.projectservice.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CommentPurchaseDTO {
    @NotNull(message = "Cotent must not be null")
    private String content;

    @NotNull(message = "Username must not be null")
    private String userName;
}
