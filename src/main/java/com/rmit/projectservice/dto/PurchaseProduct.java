package com.rmit.projectservice.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class PurchaseProduct {
    @NotNull(message = "Product code must not be null")
    private String code;

    @JsonProperty(value = "productName")
    private String name;

    private List<ProductMediaFile> mediaFiles;

    @NotNull(message = "Quantity must not be null")
    private Integer quantity;

    public PurchaseProduct(@NotNull String code, String name ,@NotNull Integer quantity,List<ProductMediaFile> mediaFiles){
        this.code = code;
        this.quantity = quantity;
        this.name = name;
        this.mediaFiles = mediaFiles;
    }
}
