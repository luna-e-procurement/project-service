package com.rmit.projectservice.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.rmit.projectservice.entity.purchaserequisition.MediaFile;
import com.rmit.projectservice.entity.purchaserequisition.PurchaseRequisitionPriority;
import com.rmit.projectservice.entity.purchaserequisition.PurchaseRequisitionStatus;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PurchaseRequisitionDTO {
    private String id;

    @NotNull(message = "Project code must not be null")
    private String projectCode;

    @NotNull(message = "Project name must not be null")
    private String purchaseName;

    @NotNull(message = "Requester must not be null")
    private String requester;

    @NotNull(message = "Product must be provide to create purchase requisition")
    private List<PurchaseProduct> products;

    private List<MediaFile> fileUploads;

    @NotNull(message = "Purchase requisition status must be provide to create purchase requisition")
    private PurchaseRequisitionPriority priority;

    private PurchaseRequisitionStatus status;

    private Boolean isApproved;
    private Boolean isRejected;

    private String targetDate;

    private String dueDate;

    private String createdDate;
}
