package com.rmit.projectservice.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.rmit.projectservice.enums.Permission;
import com.rmit.projectservice.enums.Roles;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountDTO implements Serializable {

    private Long id;
    @NotNull(message = "Email must not be null")
    @Email
    private String email;
    @NotNull(message = "Username must not be null")
    private String username;
    private String legalEntityCode;
    private Roles role;
    private String departmentCode;
    private String teamCode;
    private List<Permission> permissions;
}
