package com.rmit.projectservice.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.rmit.projectservice.enums.Roles;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class SetRoleDTO {
    @NotNull(message = "Email must not be null")
    private String userEmail;

    @NotNull(message = "Role must not be null")
    @Valid
    private Roles role;
}
