package com.rmit.projectservice.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.rmit.projectservice.entity.purchaserequisition.PurchaseRequisitionPriority;
import com.rmit.projectservice.entity.purchaserequisition.PurchaseRequisitionStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UpdateProjectDTO {
    private String name;

    private String value;

    private String purchaseAllowance;

    private PurchaseRequisitionStatus status;

    private PurchaseRequisitionPriority priority;

    private Boolean isDefault;
}
