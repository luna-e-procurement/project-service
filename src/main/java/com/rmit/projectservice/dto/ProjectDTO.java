package com.rmit.projectservice.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.rmit.projectservice.entity.project.ProjectCode;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProjectDTO {
    @NotNull(message = "Project name must not be null")
    private String name;

    @NotNull(message = "Project code must not be null")
    private ProjectCode projectCode;

    private String value;

    @NotNull(message = "Project allowance must not be null")
    private String purchaseAllowance;

    private Boolean isDefault;

    @NotNull(message = "Project must be belong to a legal entity")
    private String legalEntityCode;
}
