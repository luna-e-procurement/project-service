package com.rmit.projectservice.service;

import com.rmit.projectservice.dto.*;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface PurchaseRequisitionService {
    ResponseEntity<ResponseDTO<PurchaseRequisitionDTO>> createPurchaseRequisition(PurchaseRequisitionDTO purchaseRequisitionDTO);

    ResponseEntity<String> updatePurchaseRequisition(Long id, UpdatePurchaseRequisitionDTO updatePurchaseRequisitionDTO);

    ResponseEntity<List<PurchaseRequisitionDTO>> getPurchaseRequisitionsInProject(String projectCode);

    ResponseEntity<PurchaseRequisitionDTO> getPurchaseRequisitionDetail(String projectCode, String purchaseRequisitionId);

    ResponseEntity<String> submitPurchaseRequisition(String projectCode, String purchaseRequisitionId);

    ResponseEntity<List<PurchaseRequisitionDTO>> getPurchaseRequisitionApprovalList(String legalEntityCode);

    ResponseEntity<String> approvePurchaseRequisition(String projectCode, String purchaseRequisitionId, ApprovePurchaseDTO approvePurchaseDTO);

    ResponseEntity<PageResponse<List<PurchaseRequisitionDTO>>> getPurchaseRequisitionPageable(String legalEntityCode, String search, int page, int size, String sortBy, String sortDirection);

    ResponseEntity<String> setPurchaseRequisitionStatus(String purchaseRequisitionId, SetPurchaseRequisitionStatusDTO setPurchaseRequisitionStatusDTO);

    ResponseEntity<String> commentPurchaseRequisition(String projectCode, String purchaseRequisitionId, CommentPurchaseDTO commentPurchaseDTO);

    ResponseEntity<PageResponse<List<CommentDTO>>> getCommentInPurchaseRequisition(String projectCode, String purchaseRequisitionId, int page, int size);

    ResponseEntity<String> rejectPurchaseRequisition(String userInfo, String projectCode, String purchaseRequisitionId, RejectPurchaseDTO rejectPurchaseDTO);
}
