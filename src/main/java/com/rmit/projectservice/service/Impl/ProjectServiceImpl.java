package com.rmit.projectservice.service.Impl;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rmit.projectservice.dto.*;
import com.rmit.projectservice.entity.project.Project;
import com.rmit.projectservice.entity.purchaserequisition.PurchaseRequisition;
import com.rmit.projectservice.exception.LegalEntityNotFoundException;
import com.rmit.projectservice.exception.ProjectNotFoundException;
import com.rmit.projectservice.feignclients.AccountFeignClient;
import com.rmit.projectservice.repository.ProjectRepository;
import com.rmit.projectservice.repository.PurchaseRequisitionRepository;
import com.rmit.projectservice.service.ProjectService;
import com.rmit.projectservice.ultils.Utils;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class ProjectServiceImpl implements ProjectService {
    private final ProjectRepository projectRepository;
    private final AccountFeignClient accountFeignClient;
    private final ObjectMapper objectMapper;
    private final PurchaseRequisitionRepository purchaseRequisitionRepository;

    @Override
    public ResponseEntity<ResponseDTO<ProjectDTO>> createProject(ProjectDTO projectDTO) {
        LegalEntity legalEntity = accountFeignClient.getLegalEntityByCode(projectDTO.getLegalEntityCode());
        if (ObjectUtils.isEmpty(legalEntity)) {
            throw new LegalEntityNotFoundException("Cannot find legal entity with code: " + projectDTO.getLegalEntityCode());
        }
        if (projectRepository.findByProjectCode_Code(projectDTO.getProjectCode().getCode()).isPresent())
            return ResponseEntity.status(HttpStatus.FOUND).body(new ResponseDTO<>("Project code is already exist"));
        if (projectRepository.findByProjectCode_LabelAndLegalEntityCode(projectDTO.getProjectCode().getLabel(), projectDTO.getLegalEntityCode()).isPresent())
            return ResponseEntity.status(HttpStatus.FOUND).body(new ResponseDTO<>("Project label is already exist in legal entity"));
        Project project = objectMapper.convertValue(projectDTO,Project.class);
        projectRepository.save(project);
        return ResponseEntity.ok(new ResponseDTO<>("Successfully create project",projectDTO));
    }

    @Override
    public ResponseEntity<String> updateProject(String projectCode, UpdateProjectDTO updateProjectDTO) {
        Project project =  projectRepository.findByProjectCode_Code(projectCode).orElseThrow(() -> new ProjectNotFoundException("Cannot find any project with code: "+projectCode));
        try {
            projectRepository.save(objectMapper.updateValue(project,updateProjectDTO));
        } catch (JsonMappingException e) {
            throw new RuntimeException(e);
        }
        return ResponseEntity.ok("Project updated");
    }

    @Override
    public ResponseEntity<ProjectDTO> getProjectByCode(String projectCode) {
        Project project = projectRepository.findByProjectCode_Code(projectCode).orElseThrow(() ->new ProjectNotFoundException("Cannot find any project with code: "+projectCode));
        ProjectDTO projectDTO = objectMapper.convertValue(project,ProjectDTO.class);
        return ResponseEntity.ok(projectDTO);
    }

    @Override
    public ResponseEntity<String> markProjectAsDefault(String projectCode) {
        Project project = projectRepository.findByProjectCode_Code(projectCode).orElseThrow(() ->new ProjectNotFoundException("Cannot find any project with code: "+projectCode));
        if(project.getIsDefault())
            return ResponseEntity.ok("This project is already a default project");
        Optional<Project> defaultProject = projectRepository.findByIsDefault(Boolean.TRUE);
        if(defaultProject.isPresent()) {
            defaultProject.get().setIsDefault(Boolean.FALSE);
            projectRepository.save(defaultProject.get());
        }
        project.setIsDefault(Boolean.TRUE);
        projectRepository.save(project);
        return ResponseEntity.ok("Project mark as default");
    }

    @Override
    public ResponseEntity<PageResponse<List<ProjectDTO>>> getProjectsInLegalEntity(String legalEntityCode, String search, int page, int size, String sortBy, String sortDirection) {
        LegalEntity legalEntity = accountFeignClient.getLegalEntityByCode(legalEntityCode);
        if (ObjectUtils.isEmpty(legalEntity)) {
            throw new LegalEntityNotFoundException("Cannot find legal entity with code: " + legalEntityCode);
        }
        Sort sort;
        Pageable pageable;
        Page<Project> projects;
        if (!StringUtils.isBlank(sortBy)) sort = Sort.by(Utils.getSortDirection(sortDirection), sortBy);
        else sort = null;
        if (!ObjectUtils.isEmpty(sort)) pageable = PageRequest.of(page, size, sort);
        else pageable = PageRequest.of(page, size);
        if (!StringUtils.isBlank(search))
            projects = projectRepository.searchProjectByProjectCode_CodeAndNameAndLegalEntityCode(search, legalEntityCode, pageable);
        else projects = projectRepository.findAllByLegalEntityCode(legalEntityCode, pageable);
        Page<ProjectDTO> projectDTOS = projects.map(project -> objectMapper.convertValue(project, ProjectDTO.class));
        return ResponseEntity.ok(new PageResponse<>(projectDTOS.getContent(), projectDTOS.getPageable().getPageNumber() + 1, projectDTOS.getSize(), projectDTOS.getTotalPages(), projectDTOS.getTotalElements()));
    }

    @Override
    public ResponseEntity<String> deleteProject(String projectCode) {
        Project project = projectRepository.findByProjectCode_Code(projectCode).orElseThrow(() ->new ProjectNotFoundException("Cannot find any project with code: "+projectCode));
        List<PurchaseRequisition> purchaseRequisitions = purchaseRequisitionRepository.findByProjectCode(project.getProjectCode().getCode());
        if(CollectionUtils.isEmpty(purchaseRequisitions)){
            projectRepository.delete(project);
            return ResponseEntity.ok("Project deleted");
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Cannot delete project because it has link to some purchase requisition(s)");
    }
}