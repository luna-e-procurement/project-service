package com.rmit.projectservice.service.Impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rmit.projectservice.dto.*;
import com.rmit.projectservice.entity.PurchaseRequisitionProduct;
import com.rmit.projectservice.entity.project.Project;
import com.rmit.projectservice.entity.purchaserequisition.Comment;
import com.rmit.projectservice.entity.purchaserequisition.PurchaseRequisition;
import com.rmit.projectservice.entity.purchaserequisition.PurchaseRequisitionStatus;
import com.rmit.projectservice.exception.LegalEntityNotFoundException;
import com.rmit.projectservice.exception.ProjectNotFoundException;
import com.rmit.projectservice.exception.PurchaseRequisitionNotFoundException;
import com.rmit.projectservice.feignclients.AccountFeignClient;
import com.rmit.projectservice.feignclients.ProductFeignClient;
import com.rmit.projectservice.repository.CommentRepository;
import com.rmit.projectservice.repository.ProjectRepository;
import com.rmit.projectservice.repository.PurchaseRequisitionProductRepository;
import com.rmit.projectservice.repository.PurchaseRequisitionRepository;
import com.rmit.projectservice.service.PurchaseRequisitionService;
import com.rmit.projectservice.ultils.Utils;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PurchaseRequisitionServiceImpl implements PurchaseRequisitionService {
    private final PurchaseRequisitionRepository purchaseRequisitionRepository;
    private final ObjectMapper objectMapper;
    private final PurchaseRequisitionProductRepository purchaseRequisitionProductRepository;
    private final ProjectRepository projectRepository;
    private final ProductFeignClient productFeignClient;
    private final AccountFeignClient accountFeignClient;
    private final DateTimeFormatter dateTimeFormatter;
    private final CommentRepository commentRepository;
    private final ObjectMapper objectMapperLocalDateTime;

    @Override
    @Transactional
    public ResponseEntity<ResponseDTO<PurchaseRequisitionDTO>> createPurchaseRequisition(PurchaseRequisitionDTO purchaseRequisitionDTO) {
        Project project = projectRepository.findByProjectCode_Code(purchaseRequisitionDTO.getProjectCode()).orElseThrow(() -> new ProjectNotFoundException("Cannot find project with code: " + purchaseRequisitionDTO.getProjectCode()));
        List<PurchaseProduct> purchaseProducts = purchaseRequisitionDTO.getProducts();
        LocalDate targetDate = LocalDate.parse(purchaseRequisitionDTO.getTargetDate(), dateTimeFormatter);
        LocalDate dueDate = LocalDate.parse(purchaseRequisitionDTO.getDueDate(), dateTimeFormatter);
        purchaseRequisitionDTO.setProducts(null);
        purchaseRequisitionDTO.setTargetDate(null);
        purchaseRequisitionDTO.setDueDate(null);
        PurchaseRequisition purchaseRequisition = objectMapper.convertValue(purchaseRequisitionDTO, PurchaseRequisition.class);
        purchaseRequisition.setCreatedDate(LocalDate.now(ZoneOffset.UTC));
        purchaseRequisition.setTargetDate(targetDate);
        purchaseRequisition.setDueDate(dueDate);
        PurchaseRequisition purchaseRequisitionAfterSave = purchaseRequisitionRepository.saveAndFlush(purchaseRequisition);
        for (PurchaseProduct purchaseProduct : purchaseProducts) {
            ProductDTO productDTO = productFeignClient.getProductDetail(project.getLegalEntityCode(), purchaseProduct.getCode()).getBody();
            if (ObjectUtils.isEmpty(productDTO))
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseDTO<>("Cannot create purchase because product with code " + purchaseProduct.getCode() + " does not exist"));
            PurchaseRequisitionProduct purchaseRequisitionProduct = PurchaseRequisitionProduct.builder()
                    .purchaseRequisitionId(purchaseRequisitionAfterSave.getId())
                    .productCode(purchaseProduct.getCode())
                    .quantity(purchaseProduct.getQuantity())
                    .build();
            purchaseRequisitionProductRepository.save(purchaseRequisitionProduct);
        }
        return ResponseEntity.ok(new ResponseDTO<>("Purchase requisition is created", purchaseRequisitionDTO));
    }

    @Override
    public ResponseEntity<String> updatePurchaseRequisition(Long id, UpdatePurchaseRequisitionDTO updatePurchaseRequisitionDTO) {
        PurchaseRequisition purchaseRequisition = purchaseRequisitionRepository.findById(id).orElseThrow(() -> new PurchaseRequisitionNotFoundException("Cannot find purchase requisition with this id: " + id));
        try {
            purchaseRequisitionRepository.save(objectMapper.updateValue(purchaseRequisition, updatePurchaseRequisitionDTO));
        } catch (JsonMappingException e) {
            throw new RuntimeException(e);
        }
        return ResponseEntity.ok("Purchase requisition updated");
    }

    @Override
    public ResponseEntity<PurchaseRequisitionDTO> getPurchaseRequisitionDetail(String projectCode, String purchaseRequisitionId) {
        Project project = projectRepository.findByProjectCode_Code(projectCode).orElseThrow(() -> new ProjectNotFoundException("Cannot find project with code: " + projectCode));
        PurchaseRequisition purchaseRequisition = purchaseRequisitionRepository.findById(Long.valueOf(purchaseRequisitionId)).orElseThrow(() -> new PurchaseRequisitionNotFoundException("Cannot find purchase requisition with this id: " + purchaseRequisitionId));
        PurchaseRequisitionDTO purchaseRequisitionDTO = objectMapper.convertValue(purchaseRequisition, PurchaseRequisitionDTO.class);
        List<PurchaseRequisitionProduct> purchaseRequisitionProducts = purchaseRequisitionProductRepository.findByPurchaseRequisitionId(Long.valueOf(purchaseRequisitionId));
        List<PurchaseProduct> purchaseProducts = purchaseRequisitionProducts.stream()
                .map(purchaseRequisitionProduct -> {
                    ProductDTO productDTO = productFeignClient.getProductDetail(project.getLegalEntityCode(), purchaseRequisitionProduct.getProductCode()).getBody();
                    if (ObjectUtils.isEmpty(productDTO)) return null;
                    return new PurchaseProduct(productDTO.getCode(), productDTO.getName(), purchaseRequisitionProduct.getQuantity(), productDTO.getMediaFiles());
                }).toList();
        purchaseRequisitionDTO.setProducts(purchaseProducts);
        return ResponseEntity.ok(purchaseRequisitionDTO);
    }

    @Override
    public ResponseEntity<String> submitPurchaseRequisition(String projectCode, String purchaseRequisitionId) {
        projectRepository.findByProjectCode_Code(projectCode).orElseThrow(() -> new ProjectNotFoundException("Cannot find project with code: " + projectCode));
        PurchaseRequisition purchaseRequisition = purchaseRequisitionRepository.findById(Long.valueOf(purchaseRequisitionId)).orElseThrow(() -> new PurchaseRequisitionNotFoundException("Cannot find purchase requisition with this id: " + purchaseRequisitionId));
        purchaseRequisition.setStatus(PurchaseRequisitionStatus.WAITING_TO_APPROVAL);
        purchaseRequisitionRepository.save(purchaseRequisition);
        return ResponseEntity.ok("Purchase requisition submitted");
    }

    @Override
    public ResponseEntity<List<PurchaseRequisitionDTO>> getPurchaseRequisitionApprovalList(String legalEntityCode) {
        List<Project> projects = projectRepository.findByLegalEntityCode(legalEntityCode);
        List<PurchaseRequisition> purchaseRequisitions = new ArrayList<>();
        projects.forEach(project -> {
            purchaseRequisitions.addAll(purchaseRequisitionRepository.findByProjectCodeAndStatus(project.getProjectCode().getCode(), PurchaseRequisitionStatus.WAITING_TO_APPROVAL));
        });
        List<PurchaseRequisitionDTO> purchaseRequisitionDTOS = purchaseRequisitions.stream().map(purchaseRequisition -> objectMapper.convertValue(purchaseRequisition, PurchaseRequisitionDTO.class)).toList();
        return ResponseEntity.ok(purchaseRequisitionDTOS);
    }

    @Override
    public ResponseEntity<String> approvePurchaseRequisition(String projectCode, String purchaseRequisitionId, ApprovePurchaseDTO approvePurchaseDTO) {
        projectRepository.findByProjectCode_Code(projectCode).orElseThrow(() -> new ProjectNotFoundException("Cannot find project with code: " + projectCode));
        PurchaseRequisition purchaseRequisition = purchaseRequisitionRepository.findById(Long.valueOf(purchaseRequisitionId)).orElseThrow(() -> new PurchaseRequisitionNotFoundException("Cannot find purchase requisition with this id: " + purchaseRequisitionId));
        if (approvePurchaseDTO.getApprove()) {
            purchaseRequisition.setStatus(PurchaseRequisitionStatus.IN_PROGRESS);
            purchaseRequisitionRepository.save(purchaseRequisition);
            return ResponseEntity.ok("Purchase requisition approved");
        }
        purchaseRequisition.setStatus(PurchaseRequisitionStatus.CANCELLED);
        if (!StringUtils.isBlank(approvePurchaseDTO.getComment())) {
            List<Comment> comments = purchaseRequisition.getComments();
            comments.add(Comment.builder()
                    .content(approvePurchaseDTO.getComment())
                    .userName(approvePurchaseDTO.getUserName())
                    .createdDate(LocalDateTime.now(ZoneOffset.UTC).plusHours(7))
                    .build());
            purchaseRequisition.setComments(comments);
        }
        purchaseRequisitionRepository.save(purchaseRequisition);
        return ResponseEntity.ok("Purchase requisition cancelled");
    }

    @Override
    public ResponseEntity<PageResponse<List<PurchaseRequisitionDTO>>> getPurchaseRequisitionPageable(String legalEntityCode, String search, int page, int size, String sortBy, String sortDirection) {
        LegalEntity legalEntity = accountFeignClient.getLegalEntityByCode(legalEntityCode);
        if (ObjectUtils.isEmpty(legalEntity)) {
            throw new LegalEntityNotFoundException("Cannot find legal entity with code: " + legalEntityCode);
        }
        Sort sort = Sort.unsorted();
        Pageable pageable;
        Page<PurchaseRequisition> purchaseRequisitions;
        if (!StringUtils.isBlank(sortBy))
            sort = Sort.by(Utils.getSortDirection(sortDirection), sortBy);
        pageable = PageRequest.of(page, size, sort);
        List<Project> projects = projectRepository.findByLegalEntityCode(legalEntityCode);
        List<String> projectCodes = projects.stream().map(project -> project.getProjectCode().getCode()).toList();
        if (!StringUtils.isBlank(search)) {
            purchaseRequisitions = purchaseRequisitionRepository.searchPurchaseRequisitionByProjectCodeOrId(search, projectCodes, pageable);
        } else {
            purchaseRequisitions = purchaseRequisitionRepository.findAllByProjectCodeIn(projectCodes, pageable);
        }
        Page<PurchaseRequisitionDTO> purchaseRequisitionDTOS = purchaseRequisitions.map(purchaseRequisition -> objectMapper.convertValue(purchaseRequisition, PurchaseRequisitionDTO.class));
        return ResponseEntity.ok(new PageResponse<>(purchaseRequisitionDTOS.getContent(), purchaseRequisitionDTOS.getPageable().getPageNumber() + 1, purchaseRequisitionDTOS.getSize(), purchaseRequisitionDTOS.getTotalPages(), purchaseRequisitionDTOS.getTotalElements()));
    }

    @Override
    public ResponseEntity<String> setPurchaseRequisitionStatus(String purchaseRequisitionId, SetPurchaseRequisitionStatusDTO setPurchaseRequisitionStatusDTO) {
        PurchaseRequisition purchaseRequisition = purchaseRequisitionRepository.findById(Long.valueOf(purchaseRequisitionId)).orElseThrow(() -> new PurchaseRequisitionNotFoundException("Cannot find purchase requisition with this id: " + purchaseRequisitionId));
        if (setPurchaseRequisitionStatusDTO.getIsApproved()) {
            purchaseRequisition.setIsApproved(Boolean.TRUE);
            purchaseRequisition.setIsRejected(Boolean.FALSE);
        }
        purchaseRequisition.setStatus(setPurchaseRequisitionStatusDTO.getStatus());
        purchaseRequisitionRepository.save(purchaseRequisition);
        return ResponseEntity.ok("Purchase requisition status updated");
    }

    @Override
    public ResponseEntity<String> commentPurchaseRequisition(String projectCode, String purchaseRequisitionId, CommentPurchaseDTO commentPurchaseDTO) {
        Project project = projectRepository.findByProjectCode_Code(projectCode).orElseThrow(() -> new ProjectNotFoundException("Cannot find project with code: " + projectCode));
        PurchaseRequisition purchaseRequisition = purchaseRequisitionRepository.findById(Long.valueOf(purchaseRequisitionId)).orElseThrow(() -> new PurchaseRequisitionNotFoundException("Cannot find purchase requisition with this id: " + purchaseRequisitionId));
        Comment comment = Comment.builder()
                .content(commentPurchaseDTO.getContent())
                .userName(commentPurchaseDTO.getUserName())
                .createdDate(LocalDateTime.now(ZoneOffset.UTC).plusHours(7))
                .build();
        List<Comment> comments = purchaseRequisition.getComments();
        comments.add(comment);
        purchaseRequisition.setComments(comments);
        purchaseRequisitionRepository.save(purchaseRequisition);
        return ResponseEntity.ok("Comment posted");
    }

    @Override
    public ResponseEntity<PageResponse<List<CommentDTO>>> getCommentInPurchaseRequisition(String projectCode, String purchaseRequisitionId, int page, int size) {
        Project project = projectRepository.findByProjectCode_Code(projectCode).orElseThrow(() -> new ProjectNotFoundException("Cannot find project with code: " + projectCode));
        PurchaseRequisition purchaseRequisition = purchaseRequisitionRepository.findById(Long.valueOf(purchaseRequisitionId)).orElseThrow(() -> new PurchaseRequisitionNotFoundException("Cannot find purchase requisition with this id: " + purchaseRequisitionId));
        Pageable pageable = PageRequest.of(page, size,Sort.by("createdDate").descending());
        Page<Comment> comments = commentRepository.findCommentsByPurchaseRequisition_Id(purchaseRequisition.getId(), pageable);
        Page<CommentDTO> commentDTOS = comments.map(comment -> objectMapperLocalDateTime.convertValue(comment, CommentDTO.class));
        return ResponseEntity.ok(new PageResponse<>(commentDTOS.getContent(), commentDTOS.getPageable().getPageNumber() + 1, commentDTOS.getSize(), commentDTOS.getTotalPages(), commentDTOS.getTotalElements()));
    }

    @Override
    public ResponseEntity<String> rejectPurchaseRequisition(String userInfo, String projectCode, String purchaseRequisitionId, RejectPurchaseDTO rejectPurchaseDTO) {
        try{
            JWTPayload jwtPayload = objectMapper.readValue(userInfo, JWTPayload.class);
            projectRepository.findByProjectCode_Code(projectCode).orElseThrow(() -> new ProjectNotFoundException("Cannot find project with code: " + projectCode));
            PurchaseRequisition purchaseRequisition = purchaseRequisitionRepository.findById(Long.valueOf(purchaseRequisitionId)).orElseThrow(() -> new PurchaseRequisitionNotFoundException("Cannot find purchase requisition with this id: " + purchaseRequisitionId));
            if (StringUtils.isBlank(rejectPurchaseDTO.getComment())) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Reject must provide comment");
            }
            purchaseRequisition.setIsRejected(Boolean.TRUE);
            List<Comment> comments = purchaseRequisition.getComments();
            comments.add(Comment.builder()
                    .content(rejectPurchaseDTO.getComment())
                    .userName(jwtPayload.getUsername())
                    .createdDate(LocalDateTime.now(ZoneOffset.UTC).plusHours(7))
                    .build());
            purchaseRequisition.setComments(comments);
            purchaseRequisitionRepository.save(purchaseRequisition);
            return ResponseEntity.ok("Purchase requisition rejected");
        }
        catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ResponseEntity<List<PurchaseRequisitionDTO>> getPurchaseRequisitionsInProject(String projectCode) {
        projectRepository.findByProjectCode_Code(projectCode).orElseThrow(() -> new ProjectNotFoundException("Cannot find project with code: " + projectCode));
        List<PurchaseRequisition> purchaseRequisitions = purchaseRequisitionRepository.findByProjectCode(projectCode);
        List<PurchaseRequisitionDTO> purchaseRequisitionDTOS = purchaseRequisitions.stream().map(purchaseRequisition -> objectMapper.convertValue(purchaseRequisitions, PurchaseRequisitionDTO.class)).toList();
        return ResponseEntity.ok(purchaseRequisitionDTOS);
    }
}
