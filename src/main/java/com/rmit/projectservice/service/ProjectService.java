package com.rmit.projectservice.service;

import com.rmit.projectservice.dto.PageResponse;
import com.rmit.projectservice.dto.ProjectDTO;
import com.rmit.projectservice.dto.ResponseDTO;
import com.rmit.projectservice.dto.UpdateProjectDTO;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ProjectService {
    ResponseEntity<ResponseDTO<ProjectDTO>> createProject(ProjectDTO projectDTO);

    ResponseEntity<String> updateProject(String projectCode, UpdateProjectDTO updateProjectDTO);

    ResponseEntity<ProjectDTO> getProjectByCode(String projectCode);

    ResponseEntity<String> markProjectAsDefault(String projectCode);

    ResponseEntity<PageResponse<List<ProjectDTO>>> getProjectsInLegalEntity(String legalEntityCode, String search, int page, int size, String sortBy, String sortDirection);

    ResponseEntity<String> deleteProject(String projectCode);
}
