package com.rmit.projectservice.enums;

public enum Roles {
    VIEWER,
    MEMBER,
    SUPERVISOR,
    MANAGER,
    ADMINISTRATOR
}
