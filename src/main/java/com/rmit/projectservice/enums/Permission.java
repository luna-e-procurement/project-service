package com.rmit.projectservice.enums;

public enum Permission {
    VIEW,
    EDIT,
    CREATE
}
