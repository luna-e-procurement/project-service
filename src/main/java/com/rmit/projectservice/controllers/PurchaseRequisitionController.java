package com.rmit.projectservice.controllers;

import com.rmit.projectservice.aspect.Auth;
import com.rmit.projectservice.dto.*;
import com.rmit.projectservice.enums.Roles;
import com.rmit.projectservice.service.PurchaseRequisitionService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class PurchaseRequisitionController {
    private final PurchaseRequisitionService purchaseRequisitionService;

    @PostMapping("/purchase-requisition")
    public ResponseEntity<ResponseDTO<PurchaseRequisitionDTO>> createPurchaseRequisition(@Valid @RequestBody PurchaseRequisitionDTO purchaseRequisitionDTO) {
        return purchaseRequisitionService.createPurchaseRequisition(purchaseRequisitionDTO);
    }

    @PatchMapping("/purchase-requisition/{id}")
    public ResponseEntity<String> updatePurchaseRequisition(@PathVariable Long id, @RequestBody UpdatePurchaseRequisitionDTO updatePurchaseRequisitionDTO) {
        return purchaseRequisitionService.updatePurchaseRequisition(id, updatePurchaseRequisitionDTO);
    }

//    @GetMapping("/purchase-requisition/{projectCode}")
//    public ResponseEntity<List<PurchaseRequisitionDTO>> getPurchaseRequisitionsInProject(@PathVariable String projectCode) {
//        return purchaseRequisitionService.getPurchaseRequisitionsInProject(projectCode);
//    }

    @GetMapping("/purchase-requisition/{projectCode}/{purchaseRequisitionId}")
    public ResponseEntity<PurchaseRequisitionDTO> getPurchaseRequisition(@PathVariable String projectCode, @PathVariable String purchaseRequisitionId) {
        return purchaseRequisitionService.getPurchaseRequisitionDetail(projectCode, purchaseRequisitionId);
    }

    @PostMapping("/purchase-requisition/{projectCode}/{purchaseRequisitionId}/submit")
    public ResponseEntity<String> submitPurchaseRequisition(@PathVariable String projectCode, @PathVariable String purchaseRequisitionId) {
        return purchaseRequisitionService.submitPurchaseRequisition(projectCode, purchaseRequisitionId);
    }

    @PostMapping("/purchase-requisition/{purchaseRequisitionId}/set-status")
    public ResponseEntity<String> setPurchaseRequisitionStatus(@PathVariable String purchaseRequisitionId, @RequestBody SetPurchaseRequisitionStatusDTO setPurchaseRequisitionStatusDTO) {
        return purchaseRequisitionService.setPurchaseRequisitionStatus(purchaseRequisitionId, setPurchaseRequisitionStatusDTO);
    }

    @GetMapping("/purchase-requisition/{legalEntityCode}/approval-list")
    @Auth(role = Roles.MANAGER)
    public ResponseEntity<List<PurchaseRequisitionDTO>> getPurchaseRequisitionApprovalList(@RequestHeader("userInfo") String userInfo, @PathVariable String legalEntityCode) {
        return purchaseRequisitionService.getPurchaseRequisitionApprovalList(legalEntityCode);
    }

    @PostMapping("/purchase-requisition/{projectCode}/{purchaseRequisitionId}/approve")
    @Auth(role = Roles.MANAGER)
    public ResponseEntity<String> approvePurchaseRequisition(@RequestHeader("userInfo") String userInfo, @PathVariable String projectCode, @PathVariable String purchaseRequisitionId, @RequestBody ApprovePurchaseDTO approvePurchaseDTO) {
        return purchaseRequisitionService.approvePurchaseRequisition(projectCode, purchaseRequisitionId, approvePurchaseDTO);
    }
    @PostMapping("/purchase-requisition/{projectCode}/{purchaseRequisitionId}/reject")
    public ResponseEntity<String> rejectPurchaseRequisition(@RequestHeader("userInfo") String userInfo, @PathVariable String projectCode, @PathVariable String purchaseRequisitionId, @RequestBody RejectPurchaseDTO rejectPurchaseDTO) {
        return purchaseRequisitionService.rejectPurchaseRequisition(userInfo,projectCode, purchaseRequisitionId, rejectPurchaseDTO);
    }

    @GetMapping("/purchase-requisition/{legalEntityCode}")
    public ResponseEntity<PageResponse<List<PurchaseRequisitionDTO>>> getPurchaseRequisitionPageable(@PathVariable String legalEntityCode,
                                                                                                     @RequestParam(required = false) String search,
                                                                                                     @RequestParam(defaultValue = "1") int page,
                                                                                                     @RequestParam(defaultValue = "150") int size,
                                                                                                     @RequestParam(required = false) String sortBy, @RequestParam(defaultValue = "asc") String sortDirection) {
        return purchaseRequisitionService.getPurchaseRequisitionPageable(legalEntityCode, search, page - 1, size, sortBy, sortDirection);
    }

    @PostMapping("/purchase-requisition/{projectCode}/{purchaseRequisitionId}/comment")
    public ResponseEntity<String> commentPurchaseRequisition(@PathVariable String projectCode, @PathVariable String purchaseRequisitionId, @Valid @RequestBody CommentPurchaseDTO commentPurchaseDTO) {
        return purchaseRequisitionService.commentPurchaseRequisition(projectCode, purchaseRequisitionId, commentPurchaseDTO);
    }

    @GetMapping("/purchase-requisition/{projectCode}/{purchaseRequisitionId}/comments")
    public ResponseEntity<PageResponse<List<CommentDTO>>> getAllCommentInPurchaseRequisition(
            @RequestParam(defaultValue = "1") int page,
            @RequestParam(defaultValue = "150") int size,
            @PathVariable String projectCode, @PathVariable String purchaseRequisitionId) {
        return purchaseRequisitionService.getCommentInPurchaseRequisition(projectCode, purchaseRequisitionId,page-1,size);
    }
}