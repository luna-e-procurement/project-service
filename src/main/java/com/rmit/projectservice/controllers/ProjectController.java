package com.rmit.projectservice.controllers;

import com.rmit.projectservice.dto.PageResponse;
import com.rmit.projectservice.dto.ProjectDTO;
import com.rmit.projectservice.dto.ResponseDTO;
import com.rmit.projectservice.dto.UpdateProjectDTO;
import com.rmit.projectservice.service.ProjectService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class ProjectController {
    private final ProjectService projectService;
    @PostMapping("/project")
    public ResponseEntity<ResponseDTO<ProjectDTO>> createProject(@RequestBody @Valid ProjectDTO projectDTO){
        return projectService.createProject(projectDTO);
    }
    @PatchMapping("/project/{projectCode}")
    public ResponseEntity<String> updateProject(@RequestBody UpdateProjectDTO updateProjectDTO, @PathVariable String projectCode){
        return projectService.updateProject(projectCode,updateProjectDTO);
    }

    @GetMapping("/project/legalEntity/{legalEntityCode}")
    public ResponseEntity<PageResponse<List<ProjectDTO>>> getProjectsInLegalEntity(@PathVariable String legalEntityCode,
                                                                                  @RequestParam(required = false) String search,
                                                                                  @RequestParam(defaultValue = "1") int page,
                                                                                  @RequestParam(defaultValue = "150") int size,
                                                                                  @RequestParam(required = false) String sortBy,
                                                                                  @RequestParam(defaultValue = "asc") String sortDirection){
        return projectService.getProjectsInLegalEntity(legalEntityCode,search,page - 1,size,sortBy,sortDirection);
    }
    @GetMapping("/project/{projectCode}")
    public ResponseEntity<ProjectDTO> getProject(@PathVariable String projectCode){
        return projectService.getProjectByCode(projectCode);
    }
    @PostMapping("/project/{projectCode}/markDefault")
    public ResponseEntity<String> markProjectAsDefault(@PathVariable String projectCode){
        return projectService.markProjectAsDefault(projectCode);
    }

    @DeleteMapping("/project/{projectCode}")
    public ResponseEntity<String> deleteProject(@PathVariable String projectCode){
        return projectService.deleteProject(projectCode);
    }
}
