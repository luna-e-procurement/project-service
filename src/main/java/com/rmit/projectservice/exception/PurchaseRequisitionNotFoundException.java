package com.rmit.projectservice.exception;

public class PurchaseRequisitionNotFoundException extends RuntimeException {
    public PurchaseRequisitionNotFoundException(String message){
        super(message);
    }
}
