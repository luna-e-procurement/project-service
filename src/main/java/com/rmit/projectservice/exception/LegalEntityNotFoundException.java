package com.rmit.projectservice.exception;

public class LegalEntityNotFoundException extends RuntimeException {
    public LegalEntityNotFoundException(String message) {
        super(message);
    }
}
