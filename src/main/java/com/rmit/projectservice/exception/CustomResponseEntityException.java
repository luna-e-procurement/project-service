package com.rmit.projectservice.exception;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import feign.FeignException;
import lombok.RequiredArgsConstructor;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@ControllerAdvice
@RequiredArgsConstructor
public class CustomResponseEntityException extends ResponseEntityExceptionHandler {

    private final DateTimeFormatter dateTimeFormatter;
    private final ObjectMapper objectMapper;

    @ExceptionHandler({LegalEntityNotFoundException.class,ProjectNotFoundException.class,PurchaseRequisitionNotFoundException.class})
    public final ResponseEntity<Object> notFoundException(Exception ex, WebRequest request) {
        ErrorDetail errorDetail = new ErrorDetail(LocalDateTime.now().format(dateTimeFormatter), ex.getMessage(),
            request.getDescription(false));
        return new ResponseEntity<>(errorDetail, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({FeignException.class})
    public final ResponseEntity<Object> feignException(Exception ex, WebRequest request) {
        if(!ex.getMessage().contains("message")) {
            return new ResponseEntity<>( new ErrorDetail(LocalDateTime.now().format(dateTimeFormatter), "Server currently unavailable, try again later",
                request.getDescription(false)), HttpStatus.NOT_FOUND);
        }
        String errorMessage = ex.getMessage();
        int startIndex = errorMessage.indexOf("{");
        int endIndex = errorMessage.lastIndexOf("}") + 1;
        String jsonMessage = errorMessage.substring(startIndex, endIndex);
        String message = "";
        try {
            JsonNode jsonNode = objectMapper.readTree(jsonMessage);
            message = jsonNode.get("message").asText();
        } catch (Exception e) {
            System.err.println("Failed to parse JSON: " + e.getMessage());
        }
        ErrorDetail errorDetail = new ErrorDetail(LocalDateTime.now().format(dateTimeFormatter), message,
                request.getDescription(false));
        return new ResponseEntity<>(errorDetail, HttpStatus.NOT_FOUND);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
        HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        String errorsMessage = ex.getFieldErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).toList().toString();
        ErrorDetail errorDetail = new ErrorDetail(LocalDateTime.now().format(dateTimeFormatter),
                errorsMessage, request.getDescription(false));
        return new ResponseEntity<>(errorDetail, HttpStatus.BAD_REQUEST);
    }
}
