package com.rmit.projectservice.aspect;


import com.rmit.projectservice.enums.Permission;
import com.rmit.projectservice.enums.Roles;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value = ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Role {

    Roles role();

    Permission[] permissions() default {};
}
