package com.rmit.projectservice.feignclients;

import com.rmit.projectservice.dto.ProductDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "product-service/api")
public interface ProductFeignClient {
    @GetMapping("/product/{legalEntityCode}/{productCode}")
    ResponseEntity<ProductDTO> getProductDetail(@PathVariable String legalEntityCode, @PathVariable String productCode);
}
