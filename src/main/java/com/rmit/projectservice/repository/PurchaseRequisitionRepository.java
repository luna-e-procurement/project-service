package com.rmit.projectservice.repository;

import com.rmit.projectservice.entity.purchaserequisition.PurchaseRequisition;
import com.rmit.projectservice.entity.purchaserequisition.PurchaseRequisitionStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PurchaseRequisitionRepository extends JpaRepository<PurchaseRequisition,Long> {
    List<PurchaseRequisition> findByProjectCode(String projectCode);
    Page<PurchaseRequisition> findByProjectCode(String projectCode,Pageable pageable);
    List<PurchaseRequisition> findByProjectCodeAndStatus(String projectCode, PurchaseRequisitionStatus status);

    Page<PurchaseRequisition> findAllByProjectCodeIn(List<String> projectCodes,Pageable pageable);
    @Query("SELECT p FROM PurchaseRequisition p WHERE " +
            "p.projectCode in (:projectCodes)" +
            "and p.projectCode LIKE CONCAT('%',:search, '%')"
    )
    Page<PurchaseRequisition> searchPurchaseRequisitionByProjectCodeOrId(String search,List<String> projectCodes, Pageable pageable);

}
