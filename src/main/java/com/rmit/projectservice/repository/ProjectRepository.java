package com.rmit.projectservice.repository;

import com.rmit.projectservice.entity.project.Project;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProjectRepository extends JpaRepository<Project,Long> {
    Optional<Project> findByProjectCode_Code(String code);
    Page<Project> findByProjectCode_CodeAndLegalEntityCode(String code, String legalEntityCode, Pageable pageable);
    Optional<Project> findByProjectCode_LabelAndLegalEntityCode(String label,String legalEntityCode);
//    Optional<Project> findByCode(String code);
    Optional<Project> findByIsDefault(boolean status);

    List<Project> findByLegalEntityCode(String legalEntityCode);
    Page<Project> findAllByLegalEntityCode(String legalEntityCode,Pageable pageable);

    @Query("SELECT p FROM Project p WHERE " +
            "p.projectCode.code LIKE CONCAT('%',:search, '%')" +
            "or p.name LIKE CONCAT('%', :search, '%')" +
            "and p.legalEntityCode = :legalEntityCode"
    )
    Page<Project> searchProjectByProjectCode_CodeAndNameAndLegalEntityCode(String search, String legalEntityCode, Pageable pageable);
}
