package com.rmit.projectservice.repository;

import com.rmit.projectservice.entity.purchaserequisition.Comment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends JpaRepository<Comment,Long> {
    Page<Comment> findCommentsByPurchaseRequisition_Id(Long id, Pageable pageable);
}
