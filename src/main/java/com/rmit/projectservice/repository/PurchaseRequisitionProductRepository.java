package com.rmit.projectservice.repository;

import com.rmit.projectservice.entity.PurchaseRequisitionProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PurchaseRequisitionProductRepository extends JpaRepository<PurchaseRequisitionProduct,Long> {
    List<PurchaseRequisitionProduct> findByPurchaseRequisitionId(Long purchaseRequisitionId);
}
